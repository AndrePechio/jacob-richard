-- Projeto da discplina "Programação Concorrente/Paralela" --
 
 	Encontrar solução de um sistema matricial : Ax = b
        Método utilizado: Jacobi-Richardson

O subdiretório Sequencial contém a implementação do algoritmo Jacobi-Richardson em C na forma sequencial juntamente com o seu Makefile. O subdiretório Paralelo contém o algoritmo Jacobi-Richardson implentado em C utilizando paralelismo; o algoritmo paralelo foi implementado utilizando a biblioteca PThreads.


	Execução do código: ./EXECUTAVEL < entrada.txt
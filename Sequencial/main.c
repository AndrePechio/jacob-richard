#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int jacob_Richardson(long double** MA, long double* b, int MAX_IT, int n, long double ERROR, long double *);
long double Er(long double *xn, long double *x, int n);
int CriterioDeConvergenciaLinha(long double** MA, int i, int j, double* auxVar, int J_ORDER);


int L(int i, int j, long double** MA);
int D(int i, int j, long double** MA);
int R(int i, int j, long double** MA);

int main(){
	
	int i,j, iterations;
	int J_ORDER, J_ROW_TEST, J_MAX_IT;
	long double J_ERROR, *teste, valor_esperado, valor;
	
	long double** MA;
	long double* b, *xn;
	
	scanf(" %d",&J_ORDER);
	scanf(" %d",&J_ROW_TEST);
	scanf(" %Lf",&J_ERROR);
	scanf(" %d",&J_MAX_IT);
	
	MA = (long double**)malloc(J_ORDER*sizeof(long double*));
	teste = (long double*)malloc(J_ORDER*sizeof(long double));
		
	
	for(i=0; i < J_ORDER; i++){
		MA[i] = (long double*)malloc(J_ORDER*sizeof(long double));
		for(j=0;j< J_ORDER; j++){
			scanf(" %Lf",&MA[i][j]);
		}	
	}
	
	for(i=0;i<J_ORDER;i++)
		teste[i] = MA[J_ROW_TEST][i];
	
	b = (long double*)malloc(J_ORDER*sizeof(long double));
	xn = (long double*)malloc(J_ORDER*sizeof(long double));
		
		
	for(j=0;j<J_ORDER;j++)
		scanf(" %Lf",&b[j]);
	
	valor_esperado = b[J_ROW_TEST];
	
	iterations = jacob_Richardson(MA, b, J_MAX_IT, J_ORDER, J_ERROR, xn);
	
	valor = 0;
	for(i=0;i<J_ORDER;i++){
		valor += xn[i]*teste[i];
	}
	
	printf("-----------------------\nIteracoes: %d\nRowTest: %d => [%Lf] =? %Lf\n-----------------------\n", iterations,J_ROW_TEST,valor,valor_esperado);
	
		
	for(i=0;i<J_ORDER;i++)
		free(MA[i]);
	
	free(MA);
	free(teste);
	free(b);
	free(xn);
	
	
}

int jacob_Richardson(long double** MA, long double* b, int MAX_IT, int J_ORDER, long double ERROR, long double *xn)
{

    int iterations = 0, varCriterio, ExecutaAlg = 0;
    long double *x, erro, aux, auxVar;

    int i,j;

    x = (long double*)malloc(J_ORDER*sizeof(long double));

    erro = 0;
    for(i=0; i<J_ORDER; i++)
    {
        x[i] = 0;
    }

    do
    {
        for(i=0; i<J_ORDER; i++)
        {
            aux = 0;
            for(j=0; j<J_ORDER; j++)
            {
                if(i != j)
                {
                    aux += MA[i][j]*x[j];
                }
            }
            xn[i] = (b[i] - aux)/MA[i][i];
        }

        iterations++;

        erro = Er(xn,x,J_ORDER);

        if(erro < ERROR)
            break;

        for(i=0; i<J_ORDER; i++)
        {
            x[i] = xn[i];
        }

    }
    while(iterations <= MAX_IT);

    free(x);

    return iterations;
}

// obtem valor das matrizes diagonais auxiliares diretamente da MA

int L(int i, int j, long double** MA)
{

    if(i > j)
        return MA[i][j];
    else
        return 0;

}

int D(int i, int j, long double** MA)
{

    if(i == j)
        return MA[i][j];
    else
        return 0;

}

int R(int i, int j, long double** MA)
{

    if(i < j)
        return MA[i][j];
    else
        return 0;

}


long double Er(long double *xn, long double *x, int n)
{

    int i;
    long double maior = 0, aux, aux2, maior2 = 0, err1, err2;
	
    for(i=0; i<n; i++)
    {
        aux = fabs(xn[i] - x[i]);
        aux2 = fabs(xn[i]);
        if(i == 0)
        {
            maior = aux;
            maior2 = aux2;
        }
        else
        {
            if(aux > maior)
                maior = aux;
            if(aux2 > maior2)
                maior2 = aux2;
        }

    }
	
    err1 = maior;
    err2 = err1/maior2;

    if(err1 >= err2)
        return err2;
    else
        return err1;
}

/*retorna 0 ou 1, falso ou verdadeiro respectivamente*/
int CriterioDeConvergenciaLinha(long double** MA, int i, int j, double* auxVar, int J_ORDER)
{
    if (i != j)
    {
        if (MA[i][j] < 0)
        {
            *auxVar = *auxVar + ((MA[i][j]) * (-1));
        }
        else
        {
            *auxVar = *auxVar + MA[i][j];
        }
    }

    if ((J_ORDER - 1) == j && *auxVar < 1)
    {
        return 1;
    }

    return 0;
}


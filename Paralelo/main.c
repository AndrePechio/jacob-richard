#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>

#define THREADS_NUM 4

//parametros
typedef struct 
{
	int linha_inicial;
	int linha_final;
	int colunas;
	int id;
	
}thread_arg;

//funcs
void * jacob_paralelo(void*);
long double Er(int n);

//var globais
long double **MA;
long double *xn;
long double *x;
long double *b;

int stop, continuar;

pthread_barrier_t	barrier; // barrier synchronization object

int main(void){
		
	pthread_t th[THREADS_NUM];
    thread_arg a[THREADS_NUM];	
	
	int i,j;
	int iterations;
	int J_ORDER, J_ROW_TEST, J_MAX_IT;
	int div;
	long double J_ERROR, *teste, valor_esperado, valor, erro;
	
	pthread_barrier_init (&barrier, NULL, THREADS_NUM+1);
	
	scanf(" %d",&J_ORDER);
	scanf(" %d",&J_ROW_TEST);
	scanf(" %Lf",&J_ERROR);
	scanf(" %d",&J_MAX_IT);
	
	MA = (long double**)malloc(J_ORDER*sizeof(long double*));
	teste = (long double*)malloc(J_ORDER*sizeof(long double));
	x = (long double*)malloc(J_ORDER*sizeof(long double));
	b = (long double*)malloc(J_ORDER*sizeof(long double));
	xn = (long double*)malloc(J_ORDER*sizeof(long double));
			
	iterations = 0;
	
	for(i=0; i < J_ORDER; i++){
		MA[i] = (long double*)malloc(J_ORDER*sizeof(long double));
		x[i] = 0;
		for(j=0;j< J_ORDER; j++){
			scanf(" %Lf",&MA[i][j]);
		}	
	}
	
	for(i=0;i<J_ORDER;i++)
		teste[i] = MA[J_ROW_TEST][i];
	
	for(j=0;j<J_ORDER;j++)
		scanf(" %Lf",&b[j]);
	
	valor_esperado = b[J_ROW_TEST];	
		
	div = J_ORDER/THREADS_NUM;
	
	stop = 0;	
	
	//Criação de Threads, cada uma recebe o intervalo de linhas que irão atuar
	
	for(i=0;i < THREADS_NUM; i++){
		
		a[i].id = i + 1;
		a[i].linha_inicial = i*div;
		a[i].linha_final = a[i].linha_inicial + div;
		a[i].colunas = J_ORDER;
				
		//se for a ultima thread, coloca a linha final ate o fim da matriz, para garantir que cobrira tudo
		if(i == THREADS_NUM - 1){
			a[i].linha_final = J_ORDER;
		}
		if(pthread_create(&(th[i]), NULL, jacob_paralelo, (void *)&(a[i])))
		{
			fprintf(stderr, "Error - pthread_create()");
			exit(EXIT_FAILURE);
		}
	}
	//Loop de controle para calcular erro
	while(!stop){
		//Barreria que aguarda todas as threads atingirem a  barreira "barrier" para proseguir
		pthread_barrier_wait(&barrier);
		//calculo do erro
		erro = Er(J_ORDER);
		if(erro <= J_ERROR || iterations >= J_MAX_IT){
			stop = 1;
			//erro pequeno, parar
			// ou MAXIT foi atingido
		}
		else{
			for(i=0;i<J_ORDER;i++){
				x[i] = xn[i];
				//atualização do vetor x
			}
		}
		iterations++;
		// barreira que sinaliza que as outras threads podem continuar
		pthread_barrier_wait(&barrier);
				
		
	}
	
	for(i=0; i<THREADS_NUM; i++)
    {
        pthread_join(th[i], NULL);
		
    }	
	valor = 0;
	for(i=0;i<J_ORDER;i++){
		valor += xn[i]*teste[i];
	}
	
	printf("-----------------------\nIteracoes: %d\nRowTest: %d => [%Lf] =? %Lf\n-----------------------\n", iterations,J_ROW_TEST,valor,valor_esperado);
	
		
	for(i=0;i<J_ORDER;i++)
		free(MA[i]);
	
	free(MA);
	free(teste);
	free(b);
	free(xn);
	free(x);
	
	pthread_exit((void *)NULL);
	
}

//codigo executado por cada thread
void *jacob_paralelo(void *vargp){
	
	thread_arg *a = (thread_arg *) vargp;
	int i,j;
	long double aux;
		
	do{
		
		for(i = a->linha_inicial; i < a->linha_final; i++){
			aux = 0;
			for(j = 0; j < a->colunas; j++){
				if(i != j)
					aux += MA[i][j]*x[j];
			}
			xn[i] = (b[i] - aux)/MA[i][i];
			//preenche o vetor resposta
		}
		//sinaliza a main que o erro pode ser calculado, mas somente quando todas as threads atingirem a barreira
		pthread_barrier_wait(&barrier);
		// aguarda a main a calcular o erro
		pthread_barrier_wait(&barrier);
				
		
	}while(!stop);
		
	pthread_exit((void *)NULL);
	
}

// funcao que calcula o erro
long double Er(int n)
{

    int i;
    long double maior = 0, aux, aux2, maior2 = 0, err1, err2;
	
    for(i=0; i<n; i++)
    {
		
        aux = fabs(xn[i] - x[i]);
        aux2 = fabs(xn[i]);
        if(i == 0)
        {
            maior = aux;
            maior2 = aux2;
        }
        else
        {
            if(aux > maior)
                maior = aux;
            if(aux2 > maior2)
                maior2 = aux2;
        }

    }
	
    err1 = maior;
    err2 = err1/maior2;

    if(err1 >= err2)
        return err2;
    else
        return err1;
}
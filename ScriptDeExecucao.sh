#!/bin/bash
#Script que executa os algoritmos sequencial e paralelo
#Threads: 5 10 15 20 30
#NumExecucoes: 10
#Sequencial e Paralelo
#Matrizes: 250 500 1000 1500 2000 3000 4000


#NumeroDeThreads=5
#TamanhoMatriz=250
#ArquivoSaida=5
#Executavel=EXE5

NumeroDeThreads=0
TamanhoMatriz=0
ArquivoSaida=0
Executavel=0

NumExecucoes=10
#
function ExecutaAlgParalelo ()
{
#$1 -> Numero de Execucoes
#$2 -> Numero de Threads
#$3 -> Tamanho da Matriz
#$4 -> Nome do Executavel
#############################################################################################################################
	echo "###########################################################################################"
	echo "###########################################################################################" >> /home/grupo14a/Jacod-Richards/Paralelo/Resultado/SaidaParalelo[$2].txt
	echo "Numero de Threads: $2 Tamanho da Matriz: $3"
	echo "Numero de Threads: $2 Tamanho da Matriz: $3" >> /home/grupo14a/Jacod-Richards/Paralelo/Resultado/SaidaParalelo[$2].txt
	for (( i = 1; i <= $1; i++ )); do
		echo "Execução: $i"
		echo "Execução: $i" >> /home/grupo14a/Jacod-Richards/Paralelo/Resultado/SaidaParalelo[$2].txt
		dataInicial=$(date +"%d/%m/%Y -- %T")
		echo "Inicio: $dataInicial"
		cd /home/grupo14a/Jacod-Richards/Paralelo
		dataInicial=$(date +"%d/%m/%Y -- %T")
		/usr/bin/time -a -o /home/grupo14a/Jacod-Richards/Paralelo/Resultado/output.time.txt -p ./EXE$4 < /home/grupo14a/Jacod-Richards/matrizes/matriz$3.txt >> /home/grupo14a/Jacod-Richards/Paralelo/Resultado/SaidaParalelo[$2].txt
		dataFinal=$(date +"%d/%m/%Y -- %T")
		echo "Inicio: $dataInicial" >> /home/grupo14a/Jacod-Richards/Paralelo/Resultado/SaidaParalelo[$2].txt
		echo "Fim: $dataFinal" >> /home/grupo14a/Jacod-Richards/Paralelo/Resultado/SaidaParalelo[$2].txt
		cat /home/grupo14a/Jacod-Richards/Paralelo/Resultado/output.time.txt >> /home/grupo14a/Jacod-Richards/Paralelo/Resultado/SaidaParalelo[$2].txt
		rm /home/grupo14a/Jacod-Richards/Paralelo/Resultado/output.time.txt
		echo "Fim: $dataFinal"
		echo "--------------------------------------------------------------------------" >> /home/grupo14a/Jacod-Richards/Paralelo/Resultado/SaidaParalelo[$2].txt
	done
}

function ExecutaAlgSequencial ()
{
#$1 -> Numero de Execucoes
#$2 -> Tamanho da Matriz
#############################################################################################################################
	echo "###########################################################################################"
	echo "###########################################################################################" >> /home/grupo14a/Jacod-Richards/Sequencial/Resultado/SaidaSequencial[$2].txt
	echo "Algoritmo Sequencial Tamanho da Matriz: $2"
	echo "Algoritmo Sequencial Tamanho da Matriz: $2" >> /home/grupo14a/Jacod-Richards/Sequencial/Resultado/SaidaSequencial[$2].txt
	for (( i = 1; i <= $1; i++ )); do
		echo "Execução: $i"
		echo "Execução: $i" >> /home/grupo14a/Jacod-Richards/Sequencial/Resultado/SaidaSequencial[$2].txt
		dataInicial=$(date +"%d/%m/%Y -- %T")
		echo "Inicio: $dataInicial"
		cd /home/grupo14a/Jacod-Richards/Sequencial
		dataInicial=$(date +"%d/%m/%Y -- %T")
		/usr/bin/time -a -o /home/grupo14a/Jacod-Richards/Sequencial/Resultado/output.time.txt -p ./EXESequencial < /home/grupo14a/Jacod-Richards/matrizes/matriz$2.txt >> /home/grupo14a/Jacod-Richards/Sequencial/Resultado/SaidaSequencial[$2].txt
		dataFinal=$(date +"%d/%m/%Y -- %T")
		echo "Inicio: $dataInicial" >> /home/grupo14a/Jacod-Richards/Sequencial/Resultado/SaidaSequencial[$2].txt
		echo "Fim: $dataFinal" >> /home/grupo14a/Jacod-Richards/Sequencial/Resultado/SaidaSequencial[$2].txt
		cat /home/grupo14a/Jacod-Richards/Sequencial/Resultado/output.time.txt >> /home/grupo14a/Jacod-Richards/Sequencial/Resultado/SaidaSequencial[$2].txt
		rm /home/grupo14a/Jacod-Richards/Sequencial/Resultado/output.time.txt
		echo "Fim: $dataFinal"
		echo "--------------------------------------------------------------------------" >> /home/grupo14a/Jacod-Richards/Sequencial/Resultado/SaidaSequencial[$2].txt
	done
}

############# INICIANDO EXECUCAO ###############
echo "Inicio da Execuçao"
cd /home/grupo14a/Jacod-Richards/Paralelo
mkdir Resultado
cd /home/grupo14a/Jacod-Richards/Sequencial
mkdir Resultado



# Paralelo
echo "Executando Paralelo"
cd /home/grupo14a/Jacod-Richards/Paralelo
############################################################################################################################
### Numero de threads; 5
### Tamanho da Matriz: 250
### Arquivo Saida: 5
### Nome do Executavel: EXE5
let NumeroDeThreads=5
let TamanhoMatriz=250
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 5
### Tamanho da Matriz: 500
### Arquivo Saida: 5
### Nome do Executavel: EXE5
let NumeroDeThreads=5
let TamanhoMatriz=500
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 5
### Tamanho da Matriz: 1000
### Arquivo Saida: 5
### Nome do Executavel: EXE5
let NumeroDeThreads=5
let TamanhoMatriz=1000
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 5
### Tamanho da Matriz: 1500
### Arquivo Saida: 5
### Nome do Executavel: EXE5
let NumeroDeThreads=5
let TamanhoMatriz=1500
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 5
### Tamanho da Matriz: 2000
### Arquivo Saida: 5
### Nome do Executavel: EXE5
let NumeroDeThreads=5
let TamanhoMatriz=2000
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 5
### Tamanho da Matriz: 3000
### Arquivo Saida: 5
### Nome do Executavel: EXE5
let NumeroDeThreads=5
let TamanhoMatriz=3000
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 5
### Tamanho da Matriz: 4000
### Arquivo Saida: 5
### Nome do Executavel: EXE5
let NumeroDeThreads=5
let TamanhoMatriz=4000
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
############################################################################################################################
### Numero de threads; 10
### Tamanho da Matriz: 250
### Arquivo Saida: 10
### Nome do Executavel: EXE10
let NumeroDeThreads=10
let TamanhoMatriz=250
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 10
### Tamanho da Matriz: 500
### Arquivo Saida: 10
### Nome do Executavel: EXE10
let NumeroDeThreads=10
let TamanhoMatriz=500
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 10
### Tamanho da Matriz: 1000
### Arquivo Saida: 10
### Nome do Executavel: EXE10
let NumeroDeThreads=10
let TamanhoMatriz=1000
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 10
### Tamanho da Matriz: 1500
### Arquivo Saida: 10
### Nome do Executavel: EXE10
let NumeroDeThreads=10
let TamanhoMatriz=1500
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 10
### Tamanho da Matriz: 2000
### Arquivo Saida: 10
### Nome do Executavel: EXE10
let NumeroDeThreads=10
let TamanhoMatriz=2000
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 10
### Tamanho da Matriz: 3000
### Arquivo Saida: 10
### Nome do Executavel: EXE10
let NumeroDeThreads=10
let TamanhoMatriz=3000
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 10
### Tamanho da Matriz: 4000
### Arquivo Saida: 10
### Nome do Executavel: EXE10
let NumeroDeThreads=10
let TamanhoMatriz=4000
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
############################################################################################################################
### Numero de threads; 15
### Tamanho da Matriz: 250
### Arquivo Saida: 15
### Nome do Executavel: EXE15
let NumeroDeThreads=15
let TamanhoMatriz=250
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 15
### Tamanho da Matriz: 500
### Arquivo Saida: 15
### Nome do Executavel: EXE15
let NumeroDeThreads=15
let TamanhoMatriz=500
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 15
### Tamanho da Matriz: 1000
### Arquivo Saida: 15
### Nome do Executavel: EXE15
let NumeroDeThreads=15
let TamanhoMatriz=1000
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 15
### Tamanho da Matriz: 1500
### Arquivo Saida: 15
### Nome do Executavel: EXE15
let NumeroDeThreads=15
let TamanhoMatriz=1500
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 15
### Tamanho da Matriz: 2000
### Arquivo Saida: 15
### Nome do Executavel: EXE15
let NumeroDeThreads=15
let TamanhoMatriz=2000
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 15
### Tamanho da Matriz: 3000
### Arquivo Saida: 15
### Nome do Executavel: EXE15
let NumeroDeThreads=15
let TamanhoMatriz=3000
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 15
### Tamanho da Matriz: 4000
### Arquivo Saida: 15
### Nome do Executavel: EXE15
let NumeroDeThreads=15
let TamanhoMatriz=4000
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
############################################################################################################################
### Numero de threads; 20
### Tamanho da Matriz: 250
### Arquivo Saida: 20
### Nome do Executavel: EXE20
let NumeroDeThreads=20
let TamanhoMatriz=250
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 20
### Tamanho da Matriz: 500
### Arquivo Saida: 20
### Nome do Executavel: EXE20
let NumeroDeThreads=20
let TamanhoMatriz=500
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 20
### Tamanho da Matriz: 1000
### Arquivo Saida: 20
### Nome do Executavel: EXE20
let NumeroDeThreads=20
let TamanhoMatriz=1000
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 20
### Tamanho da Matriz: 1500
### Arquivo Saida: 20
### Nome do Executavel: EXE20
let NumeroDeThreads=20
let TamanhoMatriz=1500
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 20
### Tamanho da Matriz: 2000
### Arquivo Saida: 20
### Nome do Executavel: EXE20
let NumeroDeThreads=20
let TamanhoMatriz=2000
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 20
### Tamanho da Matriz: 3000
### Arquivo Saida: 20
### Nome do Executavel: EXE20
let NumeroDeThreads=20
let TamanhoMatriz=3000
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 20
### Tamanho da Matriz: 4000
### Arquivo Saida: 20
### Nome do Executavel: EXE20
let NumeroDeThreads=20
let TamanhoMatriz=4000
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
############################################################################################################################
### Numero de threads; 30
### Tamanho da Matriz: 250
### Arquivo Saida: 30
### Nome do Executavel: EXE30
let NumeroDeThreads=30
let TamanhoMatriz=250
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 30
### Tamanho da Matriz: 500
### Arquivo Saida: 30
### Nome do Executavel: EXE30
let NumeroDeThreads=30
let TamanhoMatriz=500
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 30
### Tamanho da Matriz: 1000
### Arquivo Saida: 30
### Nome do Executavel: EXE30
let NumeroDeThreads=30
let TamanhoMatriz=1000
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 30
### Tamanho da Matriz: 1500
### Arquivo Saida: 30
### Nome do Executavel: EXE30
let NumeroDeThreads=30
let TamanhoMatriz=1500
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 30
### Tamanho da Matriz: 2000
### Arquivo Saida: 30
### Nome do Executavel: EXE30
let NumeroDeThreads=30
let TamanhoMatriz=2000
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 30
### Tamanho da Matriz: 3000
### Arquivo Saida: 30
### Nome do Executavel: EXE30
let NumeroDeThreads=30
let TamanhoMatriz=3000
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel
############################################################################################################################
### Numero de threads; 30
### Tamanho da Matriz: 4000
### Arquivo Saida: 30
### Nome do Executavel: EXE30
let NumeroDeThreads=30
let TamanhoMatriz=4000
Executavel=$NumeroDeThreads

ExecutaAlgParalelo $NumExecucoes $NumeroDeThreads $TamanhoMatriz $Executavel

############################################################################################################################
##############################  EXECUTAR SEQUENCIAL ########################################################################
# Sequencial
echo "Executando Sequencial"
cd /home/grupo14a/Jacod-Richards/Sequencial
############################################################################################################################
### Tamanho da Matriz: 250
let TamanhoMatriz=250

ExecutaAlgSequencial $NumExecucoes $TamanhoMatriz
############################################################################################################################
### Tamanho da Matriz: 500
let TamanhoMatriz=500

ExecutaAlgSequencial $NumExecucoes $TamanhoMatriz
############################################################################################################################
### Tamanho da Matriz: 1000
let TamanhoMatriz=1000

ExecutaAlgSequencial $NumExecucoes $TamanhoMatriz
############################################################################################################################
### Tamanho da Matriz: 1500
let TamanhoMatriz=1500

ExecutaAlgSequencial $NumExecucoes $TamanhoMatriz
############################################################################################################################
### Tamanho da Matriz: 2000
let TamanhoMatriz=2000

ExecutaAlgSequencial $NumExecucoes $TamanhoMatriz
############################################################################################################################
### Tamanho da Matriz: 3000
let TamanhoMatriz=3000

ExecutaAlgSequencial $NumExecucoes $TamanhoMatriz
############################################################################################################################
### Tamanho da Matriz: 4000
let TamanhoMatriz=4000

ExecutaAlgSequencial $NumExecucoes $TamanhoMatriz
############################################################################################################################
echo "Fim da Execução"
#